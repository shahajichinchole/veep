
from django.db import models

# Create your models here.


    
    
class Role(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name



class Department(models.Model):
    name = models.CharField(max_length=200,default='IT')
    #role = models.ForeignKey(Role,on_delete=models.CASCADE)
    location = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Employee(models.Model):
    First_name = models.CharField(max_length=100,null=False)
    Last_name  = models.CharField(max_length=100,null=False)
    dept       = models.ForeignKey(Department,on_delete=models.CASCADE)
    salary     = models.IntegerField()
    bonus      = models.IntegerField()
    role       = models.ForeignKey(Role,on_delete=models.CASCADE)  

    def __str__(self):
        return "%s %s %s %s" %(self.First_name,self.Last_name,self.dept,self.role)