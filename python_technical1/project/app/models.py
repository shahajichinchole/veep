
from django.db import models

# Create your models here.
class user(models.Model):
    First_name = models.CharField(max_length=180)
    Last_name = models.CharField(max_length=180)
    Email     = models.EmailField()
    active = models.BooleanField(default= True)

    def __str__(self):
        return self.First_name

class Events(models.Model):

    Event_Title = models.CharField(max_length=500)
    user        = models.OneToOneField(user,on_delete=models.CASCADE)
    location    = models.CharField(max_length=200)
    start_event = models.DateTimeField()
    end_event   = models.DateTimeField()


    #Events = UserManager()
    


    class Meta:
        
        verbose_name_plural = "Events"

    def __str__(self):
        return self.Event_Title