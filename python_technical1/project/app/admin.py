from email.headerregistry import Group
from django.contrib import admin
from .models import user,Events
from django.contrib import messages
from django.contrib.auth.models import Group,User
from django.contrib.auth.models import Permission


# Register your models here.


admin.site.site_header = "Event Dashboard"

class UserAdmin(admin.ModelAdmin):
    list_display = ('First_name','Last_name', 'active')
  
    def active(self, obj):
        return obj.is_active == 1
  
    active.boolean = True
  
    def make_active(modeladmin, request, queryset):
        queryset.update(is_active = 1)
        messages.success(request, "Marked as Active Successfully !!")
  
    def make_inactive(modeladmin, request, queryset):
        queryset.update(is_active = 0)
        messages.success(request, "Marked as Inactive Successfully !!")
  
    admin.site.add_action(make_active, "Active")
    admin.site.add_action(make_inactive, "Inactive")
  
    # def has_delete_permission(self, request, obj = None):
    #     return False








class EventsAdmin(admin.ModelAdmin):
    #fields = (('Event_Title'),('start_event','end_event','location'))

    list_display = ['Event_Title','user','start_event','end_event','location']
    


#admin.site.register(user,UserAdmin)
admin.site.register(Events,EventsAdmin)
  
admin.site.register(user, UserAdmin)
admin.site.unregister(Group)
admin.site.unregister(User)



