#What is Inhertance ?

'''Inheritance allows us to define a class that inherits
all the methods and properties from another class.'''

#Advantage of Inheritance

'''
1.code Reusbility
2. reduces the programmer efforts
3.readability of a code
4.Low development time and cost'''


#Types of Inheritance

# 1.Single Inheritance 
# 2.Multilevel Inheritance
# 3.Multiple Inheritance
# 4.hybrid Inheritance
# 5.Hierarchical inheritance

# Single Inheritance
#When child class is derived from one parent class. This is called single inheritance. 

#syntax of single inheritance
'''class Father:
    pass
class child(Father):
    pass

ob = child()'''


#example 2
'''
class Father:
    x = input("Enter the father")

class child(Father):

    pass

ob = child()

print(ob.x)
'''

#example 3

# class Father():
#     def Inputdata(self):
#         self.name = input("Enter the name of father")
#         self.age = float(input("Enter the age of father"))
  
        
        

# class child(Father):
#     def show(self):
# #         print("name of father is",self.name,"and his age is",self.age)


# ob1 = child()
# ob1.show()

'''
class Father:
    
    def __init__(self,name,age):
        self.name = name
        self.age = age 

    def show(self):
        print(self.name)
        print(self.age)

    


class child(Father):
    def __init__(self, name, age,place):
        Father.__init__(self,name, age)
        self.place = place
    
    def show1(self):
        print(self.name)
        print(self.age)
 
        print(self.place)

c = child("shahaji",30,"latur")



c.show()
c.show1()
print(c.__dict__)
setattr(c,"name","sandhya")
setattr(c,"age",25)
c.show1()
print(c.__dict__)'''