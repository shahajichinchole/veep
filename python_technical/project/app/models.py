
from django.db import models
from django.contrib.auth.models import AbstractBaseUser



# Create your models here.

class user(AbstractBaseUser):

    First_name  = models.CharField(max_length=60)
    Last_name   = models.CharField(max_length=60)
    email       = models.EmailField(max_length=200,unique=True)        
    active      = models.BooleanField(default=True) # login is reguired
    staff       = models.BooleanField(default=False) # It's for only staff not admin
    admin       = models.BooleanField(default=False)


    REQUIRED_FIELDS = ['First_name','Last_name']
    USERNAME_FIELD  = 'email'

    def __str__(self):
        return self.First_name
    

    def get_First_name(self):
        return self.First_name


    @property
    def is_staff(self):
        return self.staff

    @property
    def is_active(self):
        return self.active

    @property
    def is_admin(self):
        return self.admin







class Events(models.Model):

    Event_Title = models.CharField(max_length=500)
    user        = models.ForeignKey(user,on_delete=models.CASCADE)
    location    = models.CharField(max_length=200)
    start_event = models.DateTimeField()
    end_event   = models.DateTimeField()

    def __str__(self):
        return self.Event_Title



    class Meta:
        
        verbose_name_plural = "Events"



'''
class user(models.Model):
    First_name = models.CharField(max_length=90)
    Last_name = models.CharField(max_length=90)
    Event = models.ForeignKey(Events,on_delete=models.CASCADE)

    class Meta:
        db_table = ''
        managed = True
        verbose_name = 'user'
        verbose_name_plural = 'users'
        
        '''
     
        