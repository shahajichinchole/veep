from django import forms
from app.models import Events
from django.contrib.auth.forms import forms


class EventForm(forms.ModelForm):
    
    class Meta:
        models = Events
        fields = "__all__"
