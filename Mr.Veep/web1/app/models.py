from django.db import models
from django.contrib.auth.models import User

# Create your models here.

    

class Fresher(models.Model):
    name = models.ForeignKey(User, on_delete = models.DO_NOTHING, default=1)
    upload_resume = models.FileField(default=None)
    
    def __str__(self):
        return self.upload_resume

class Experienced(models.Model):
    
    job_title =  models.CharField(max_length=200)
    department = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    upload_resume = models.FileField(default=None)
    name = models.ForeignKey(User, on_delete = models.DO_NOTHING, default=1)

    def __str__(self):
        return self.job_title


class EducationalDetails(models.Model):
    
        Highest_Qualification=  models.CharField(max_length=200)
        Institution_Name  = models.CharField(max_length=200)
        University_Name = models.CharField(max_length=200)
        Result = models.FloatField()
        City = models.CharField(max_length=200)
        State = models.CharField(max_length=200)
        Country = models.CharField(max_length=200)
        Upload_Marksheet = models.FileField(default=None)
        name = models.ForeignKey(User, on_delete = models.DO_NOTHING, default=1)

        def __str__(self):
            return self.Highest_Qualification
