from django import forms
from django.contrib.auth.forms import UserCreationForm #,AuthenticationForm
from django.contrib.auth.models import User
from .models import *
# from django.forms import ModelForm


class BasicDetailsForm(UserCreationForm):
    class Meta:
        model = User
        fields =["first_name","last_name","email","username","password1","password2"]
         
class FresherForm(forms.ModelForm):
    class Meta:
        model = Fresher
        fields =["upload_resume"]

        
class ExperiencedForm(forms.ModelForm):
    class Meta:
        model = Experienced
        fields =["job_title","department","company_name","location","upload_resume"]


class EducationalDetailsForm(forms.ModelForm):
    class Meta:
        model = EducationalDetails
        fields = ["Institution_Name","University_Name","Highest_Qualification","City","State","Upload_Marksheet","Result","Country"]

        Highest_Qualification =  forms.CharField(max_length=200)
        Institution_Name = forms.CharField(max_length=200)
        University_Name = forms.CharField(max_length=200)
        Result = forms.FloatField()
        City = forms.CharField(max_length=200)
        State = forms.CharField(max_length=200)
        Country = forms.CharField(max_length=200)
        Upload_Marksheet = forms.FileField()
