from distutils.log import info
from django.shortcuts import render,redirect
from .forms import *
from .models import *
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
# Create your views here.

def Register(request):
    form = BasicDetailsForm()
    if request.method=='POST':
        form = BasicDetailsForm(request.POST)
        if form.is_valid():
            form.save()
            User = form.cleaned_data.get("username")
            messages.success(request,"Account was created for "+ User)
            return redirect(LogInPage)
        
    else:
        form = BasicDetailsForm()
    return render(request,"home.html",{"form":form})

@login_required(login_url="LogInPage")
def WorkDetails(request):
    return render(request,"nework.html")

@login_required(login_url="LogInPage")
def fresher(request):
    form = FresherForm()
    if request.method=='POST':
        form = FresherForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.name = request.user
            instance.save()
            return redirect(Profile)
    else:
        form = FresherForm()
    return render(request,"fresh.html",{"form":form})


@login_required(login_url="LogInPage")
def experienced(request):
    form = ExperiencedForm()
    if request.method=='POST':
        form = ExperiencedForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.name = request.user
            instance.save()
            return redirect(Profile)
    else:
        form = ExperiencedForm()
    return render(request,"exp.html",{"form":form})



def LogInPage(request):

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect(educationalDetails)
            
        else:
            messages.info(request,"Username or Password is incorrect")
        
    return render(request,"login.html")

def LogOut(request):
    logout(request)
    return redirect(LogInPage)

@login_required(login_url="LogInPage")
def educationalDetails(request):
    form = EducationalDetailsForm()
    if request.method=='POST':
        form = EducationalDetailsForm(request.POST , request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.name = request.user
            instance.save()
            return redirect(WorkDetails)
    else:
        form = EducationalDetailsForm()

    return render(request,"edu.html",{"form":form})

@login_required(login_url="LogInPage")
def Profile(request):
    us = User.objects.filter(username=request.user)
    work = Experienced.objects.filter(name=request.user)
    frsh = Fresher.objects.filter(name = request.user)
    ed = EducationalDetails.objects.filter(name=request.user)

    context = {'us':us,"work":work,"ed":ed,"frsh":frsh}
    return render(request,"profile.html",context)

@login_required(login_url="LogInPage")
def Update(request,id):
    upd =  Experienced.objects.get(id=id)
    Update = ExperiencedForm(request.POST or None, request.FILES or None,instance=upd)
    if Update.is_valid():
        Update.save()
        return redirect(Profile)
    return render(request,"exp.html",{"Update":Update})