from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Fresher)
admin.site.register(Experienced)
admin.site.register(EducationalDetails)
