"""web1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app import views
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    path('admin/', admin.site.urls),
    path("",views.Register,name= "Register"),
    path("WorkDetails/",views.WorkDetails,name="WorkDetails"),
    path("WorkDetails/1/",views.fresher,name="fresher"),
    path("WorkDetails/2/",views.experienced,name="experienced"),
    path("educationalDetails/",views.educationalDetails, name="educationalDetails"),
    path("LogInPage/",views.LogInPage,name="LogInPage"),
    path("LogOut/",views.LogOut,name="LogOut"),
    path("Profile/",views.Profile,name="Profile"),
    path("Update/<int:id>",views.Update,name="Update"),
    
]
urlpatterns+=static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)