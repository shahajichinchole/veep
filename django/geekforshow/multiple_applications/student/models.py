
from pyexpat import models
from django.db import models

# Create your models here.
class Student(models.Model):
    name = models.CharField(max_length=180,default=None)
    roll_no = models.IntegerField()
