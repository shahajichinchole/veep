from django.db import models

# Create your models here.

class Work_info(models.Model):
    Recent_job_tittle = models.CharField(max_length=120)
    Department = models.CharField(max_length=120)
    Company_name = models.CharField(max_length=120)
    location = models.CharField(max_length=120)
    start_date = models.DateField()
    End_date = models.DateField()