from django.apps import AppConfig


class WorkDetailsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'work_details'
