from django.contrib import admin
from .models import Work_info
# Register your models here.
class work_infoAdmin(admin.ModelAdmin):

    list_display = ['Recent_job_tittle',"Department",'Company_name','location','start_date','End_date']

admin.site.register(Work_info,work_infoAdmin)
   