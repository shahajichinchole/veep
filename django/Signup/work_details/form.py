from django.forms import fields
from .models import Work_info

from django import forms

class work_infoForm(forms.ModelForm):
    class Meta:
        model = Work_info
        fields = "__all__"

