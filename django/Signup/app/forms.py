from django import forms
from django.contrib.auth import models
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, UsernameField
from django.contrib.auth.models import User
from .models import Location  




class SignUpForm(UserCreationForm):

    class  Meta:
        model = User
        fields = ["first_name","last_name","email","username","password1","password2"]


class LocationForm(forms.ModelForm):

    class  Meta:
        model = Location
        fields = "__all__"

    first_name = forms.CharField( max_length=60,required=60,widget=forms.TextInput(attrs={'Placeholder':"First Name"}))


    last_name = forms.CharField(max_length=60, required=True,widget=forms.TextInput(attrs={'Placeholder':"Last Name"}))
   # country  = forms.CharField(max_length=60, required=True,widget=forms.TextInput({ "placeholder":"country"}))
   # State = forms.CharField(max_length=60, required=True,widget=forms.TextInput(attrs={ "placeholder":"state"}))
    username = forms.CharField( max_length=60,required=60,widget=forms.TextInput(attrs={'Placeholder':"First Username"}))
    email = forms.EmailField(required=True,widget=forms.EmailInput(attrs={"class":"form-control"}))
    password1 = forms.CharField(label="Password",widget=forms.PasswordInput (attrs={ "class":"form-control"}))
    password2 = forms.CharField(label="Confirm Password",widget=forms.PasswordInput(attrs={ "class":"form-control"}))



class LoginForm(AuthenticationForm):
  username = UsernameField(widget=forms.TextInput())
  password = forms.CharField(label = "Password",widget=forms.PasswordInput())

        