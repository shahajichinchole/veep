from django.shortcuts import redirect, render
from .forms import *
from django.contrib import auth, messages

# Create your views here.
def Home(request):
    return render(request,"app/home.html")


def  SignUpView(request):
    # fm = SignUpForm()
    # form = LocationForm()

    # if fm.is_valid and form.is_valid():
    #     t = {"fm":fm ,"form":form}

    
    #return render(request,"app/signup.html",{"form":fm})


    if request.method == "POST":

        fm = SignUpForm(request.POST)
        if fm.is_valid():
            fm.save()
            messages.success(request,"Your registration form is succuess")
            return redirect(Login)
    else:
        fm = SignUpForm()

    return render(request,"app/signup.html",{"form":fm})


def Login(request):
    #if request.method == "POST":
        a = LoginForm(request.POST)

        # username = request.POST["username"]
        # password = request.POST["password"]

        # x = auth.authenticate(username=username,password=password)

        # if x is None:
        #     return redirect(Login)
        
        # else:
        #     return redirect(Home)
        return render(request,"app/login.html",{"a":a})