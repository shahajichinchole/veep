from django.db import models
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,BaseUserManager

# Create your models here.



class User(AbstractBaseUser,PermissionsMixin):

    First_name  = models.CharField(max_length=60,unique=True)
    Last_name   = models.CharField(max_length=60)
    email       = models.EmailField(max_length=200,unique=True)        
    active      = models.BooleanField(default=True) # login is reguired
    staff       = models.BooleanField(default=False) # It's for only staff not admin
    admin       = models.BooleanField(default=False)










class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_active=True, is_staff=False, is_admin=False):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")

        user_obj = self.model(
            email = self.normalize_email(email)
        )
        user_obj.set_password(password) # change user password
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active 
        user_obj.save()
        return user_obj

    def create_staffuser(self,email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_staff=True
        )
        return user

    
    def create_superuser(self, email, password=None):
        user = self.create_user(
                email,
                password=password,
                is_staff=True,
                is_admin=True
                            )
        return user




class Events(models.Model):

    Event_Title = models.CharField(max_length=500)
    user        = models.OneToOneField(User,on_delete=models.CASCADE)
    location    = models.CharField(max_length=200)
    start_event = models.DateTimeField()
    end_event   = models.DateTimeField()


 
    


    class Meta:
        
        verbose_name_plural = "Events"

    def __str__(self):
        return self.Event_Title




