from django.contrib import admin

from app.models import Events,User

# Register your models here.

class EventsAdmin(admin.ModelAdmin):
   

    list_display = ['Event_Title','user','start_event','end_event','location']